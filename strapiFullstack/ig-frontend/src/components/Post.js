import React from 'react'
/*
const post = {
    "id": 1,
    "description": "Programming is about languages, of course, but also much more. Along with good languages, programmers need toolsets to support coding: software development kits (SDKs), command-line utilities for source-code inspection and even editing, package managers, repositories targeted at developers, and so on. The ten articles listed below cover programming in this broad sense.",
    "likes": 20,
    "author": null,
    "created_at": "2020-07-04T18:49:16.101Z",
    "updated_at": "2020-07-04T18:49:16.101Z",
    "image": {
        "id": 1,
        "name": "images.jpg",
        "hash": "e7b20f934bf541c4a0cc5f16623b5898",
        "sha256": "HTpKjTofY30DbHIlhBVde7u7zVGNkuZApcM7ZGUHudo",
        "ext": ".jpg",
        "mime": "image/jpeg",
        "size": 11.23,
        "url": "/uploads/e7b20f934bf541c4a0cc5f16623b5898.jpg",
        "provider": "local",
        "provider_metadata": null,
        "created_at": "2020-07-04T18:49:16.113Z",
        "updated_at": "2020-07-04T18:49:16.113Z"
    }
}
*/
const API_URL = 'http://localhost:1337'//api image

const formatImageUrl = (url) => `${API_URL}${url}`//api image function line 38

export default ({description, likes, url}) => {//props values 
     
    

   
    return (
        <div className="Post">
            <img className="Post__Image" src={formatImageUrl(url)}alt="programming" />
            <h4>{description}</h4>
            <div><span>Likes: {likes}</span></div>
        </div>
    )
}
    
