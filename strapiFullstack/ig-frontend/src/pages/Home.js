import React,{useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import Post from "../components/Post";

export default () => {
    
    const [posts, setPosts] = useState([]);

    useEffect(() => {
      const getPosts = async () => {
        const res = await fetch("http://localhost:1337/posts"); //connect react with strapi, frontend with back end API
        const data = await res.json();
        setPosts(data);
      };
      getPosts();
    }, []);

    return (
      <div className="Home">
        {posts.map((
          post //posts all post .map do something a return a list ,map recieves a func with parm post, maps all the post
        ) => (//creates links for all post
          <Link to={`/${post.id}`}>
            <Post
              likes={post.likes}
              description={post.description}
              url={post.image && post.image.url}
            />
          </Link>
        ))}
      </div>
    );
}