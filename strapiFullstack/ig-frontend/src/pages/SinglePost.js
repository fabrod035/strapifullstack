import React, {useState, useEffect} from 'react';
import Post from '../components/Post'

export default ({match}) => {
    
    const {id} = match.params
    console.log("id ", id);

    const [post, setPost] = useState({})
    const [loading, setLoading] = useState(true)

    useEffect (() => {
        const fetchPost = async () => {
            const res = await fetch(`http://localhost:1337/post/${id}`)
            const data = await res.json()

            console.log("data ",data)
            setPost(data)
            setLoading(false)
        }
        fetchPost()

    }, [])

    return (
      <div>
        <Post
          likes={post.likes}
          description={post.description}
          url={post.image && post.image.url}
        />
      </div>
    );
}
   
